package com.frienmo;

import com.frienmo.model.CaoliuPost;
import com.spreada.utils.chinese.ZHConverter;
import org.apache.commons.io.FileUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

/**
 * Created by 4 on 2015-10-08.
 */
public class CaoLiuCrawler {
    private static final Logger log = Logger.getLogger(CaoLiuCrawler.class);

    private static int intervalSeconds;
    private static int initalPage;
    private static String baseAddress;
    private static String baseUrl;

    private static String titleHtmlElement;
    private static String postClassName;
    private static String postDateClassName;
    private static String postDateSimpleDateformat;
    private static String postUserClassName;


    //TODO
    private String threadId = "thread0806";

    private List<CaoliuPost> posts;


    public CaoLiuCrawler() {
        this(new File("config/caoliu.properties"));
    }

    public CaoLiuCrawler(File caoliuConfig) {
        Properties props = new Properties();
        try {
            props.load(new FileInputStream(caoliuConfig));

            intervalSeconds = Integer.valueOf(props.getProperty("interval.second"));
            initalPage = Integer.valueOf(props.getProperty("initial.page"));
            baseAddress = props.getProperty("address");
            baseUrl = "http://" + baseAddress + "/";

            titleHtmlElement = props.getProperty("post.title");
            postClassName = props.getProperty("post.class").replaceAll(" ",".");
            postDateClassName = props.getProperty("post.date.class").replaceAll(" ",".");
            postDateSimpleDateformat = props.getProperty("post.date.format");
            postUserClassName = props.getProperty("post.user.name");
        } catch (IOException e) {
            e.printStackTrace();
        }
        log.info("Started with intervalSeconds:" + intervalSeconds + " initalPage:" + initalPage);
        log.info("URL:" + baseUrl);
//        try {
////            this.fetchPage(7);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//
//        // test
//        try {
//            this.parserPage(FileUtils.readFileToString(new File("d.html")), 7);
//
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
    }

    public List<CaoliuPost> getPosts() {
        if (this.posts == null) {
            this.posts = new ArrayList<CaoliuPost>();
            try {
                String html = this.fetchPage(7);
                this.parserPage(html,7);
            } catch (Exception e) {
                e.printStackTrace();
                log.error(e);
            }
        }
        return this.posts;
    }

    public void cleanResults() {
        this.posts = null;
    }
//    curl "http://www.t66y.com/thread0806.php?fid=7" -H "Accept-Encoding: gzip, deflate, sdch" -H "Accept-Language: zh-CN,zh;q=0.8,en-US;q=0.6,en;q=0.4,fr;q=0.2" -H "Upgrade-Insecure-Requests: 1" -H "User-Agent: Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/45.0.2454.101 Safari/537.36" -H "Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8" -H "Referer: http://www.t66y.com/index.php" -H "Cookie: __cfduid=d0fccefb0f499d08f6ab664ba0bfe70401431976358; visid_incap_461376=cVk0VMCSRWGKrZXmSLotHlhPZ1UAAAAAQUIPAAAAAAAg7ewNe/Ls7dTNqJWHDnbD; CNZZDATA950900=cnzz_eid"%"3D1745287725-1441481666-http"%"253A"%"252F"%"252Fwww.t66y.com"%"252F"%"26ntime"%"3D1444253495" -H "Connection: keep-alive" --compressed
    private String fetchPage(int i) throws Exception {
        HttpClient client = HttpClientBuilder.create().build();
        String url = baseUrl + threadId + ".php?fid=" + i;
        HttpGet request = new HttpGet(url);

        // add request header
        request.addHeader("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/45.0.2454.101 Safari/537.36");
        HttpResponse response = client.execute(request);

        System.out.println("Response Code : "
                + response.getStatusLine().getStatusCode());
        log.debug("------------fetch:" + url);
        BufferedReader rd = new BufferedReader(
//                new InputStreamReader(response.getEntity().getContent(),"gb2312"));
//                new InputStreamReader(response.getEntity().getContent(),"CP936"));
                new InputStreamReader(response.getEntity().getContent(),"GBK"));
//                new InputStreamReader(response.getEntity().getContent()));
        StringBuffer result = new StringBuffer();
        String line = "";
        while ((line = rd.readLine()) != null) {
            result.append(line);
        }
        ZHConverter converter = ZHConverter.getInstance(ZHConverter.SIMPLIFIED);
//        System.out.println(converter.convert(result.toString()));
        String html = converter.convert(result.toString());
//        FileUtils.writeStringToFile(new File("d.html"), result.toString());
        return html;
    }

    private void parserPage(String html, int fid) {
        log.debug("-----------parser");
        Document doc = Jsoup.parse(html);
        Elements rows = doc.select("tr." + postClassName);
//        System.out.println(rows.size());
        for(Element row : rows) {
            CaoliuPost caoliuPost = buildPost(row,fid);
            if(caoliuPost != null) {
//                System.out.println(caoliuPost);
                log.debug(caoliuPost);
                this.posts.add(caoliuPost);
            }
        }
    }

    /**
     *
     * @param element tr element
     * @return Null for top-marker, too early post by config
     */
    private static CaoliuPost buildPost(Element element, int fid) {
//        System.out.println();
        String postDate = element.select("div." + postDateClassName).text();
        String title = element.select(titleHtmlElement).first().text();
        Element usernameElement = element.select("td." + postUserClassName).first().select("a").first();
        if (usernameElement == null) {
            return null;
        }
        String username = usernameElement.text();
        String color = "black";
        Element fontElement = element.select(titleHtmlElement).select("a font").first();
        if (fontElement != null) {
            color = fontElement.attr("color");
//            if (color != "") {
//                System.out.println(title);
//                System.out.println(color);
//            }
        }
//        System.out.println(title);
//        System.out.println(username);
//        System.out.println(postDate);

        int note = findNote(element);
//        if (note != 0) {
//            System.out.println(title);
//            System.out.println(note);
//        }
        int[] ids = findIds(element);
//        System.out.println(ids[0] + "/" + ids[1]);

        SimpleDateFormat sdf = new SimpleDateFormat(postDateSimpleDateformat);
        try {
            sdf.parse(postDate);
        } catch (ParseException e) {
//            e.printStackTrace();
            if (postDate.equals("") || postDate.equals("Top-marks"))
                return null;
            log.warn(postDate + " is not valid date.");
            return null;
        }
        CaoliuPost caoliuPost = new CaoliuPost(ids[0],ids[1],fid,title,username,postDate,color,note);

        return caoliuPost;
    }

    private static int findNote(Element element) {
        Element secondTdElement = element.child(1);
        if (secondTdElement == null)
            return 0;
        Element secondElement;
        try {
            secondElement = secondTdElement.child(1); // font or i
        } catch (IndexOutOfBoundsException e) {
            return 0;
        }
        if (secondElement == null)
            return 0;
        String tagname = secondElement.tagName();
        if (tagname.equals("font") && secondElement.attr("color").equalsIgnoreCase("green")) {
//            System.out.println(secondElement.text());
            String note = secondElement.text();
            int i = note.indexOf("+");
            note = note.substring(i+1,note.length()-1);
//            System.out.println(note);
            return Integer.valueOf(note);
        } else {
            return 0;
        }
    }

    private static int[] findIds(Element element) {
        Element aElement = element.select(titleHtmlElement).select("a").first();
        String url = aElement.attr("href");
        String[] parts = url.split("/");
        int[] ids = new int[2];
        try {
            ids[0] = Integer.valueOf(parts[2]);
            ids[1] = Integer.valueOf(parts[3].split("\\.")[0]);
        } catch (ArrayIndexOutOfBoundsException e) {
//            return ids;
        }
        return ids;
    }
}
