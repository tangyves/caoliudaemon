package com.frienmo.dao;

import com.frienmo.model.CaoliuPost;

import java.util.List;

/**
 * Created by 4 on 2015-10-12.
 */
public interface CaoliuDaemonDAO {

    public int insertPost(CaoliuPost caoliuPost);
    public CaoliuPost getPostByIds(int fid,int ida,int idb);
    public int updatePost(CaoliuPost caoliuPost);
    public List<CaoliuPost> findPostsByUserName(String username,int limit);
    public List<CaoliuPost> findPostsHighLight(int limit);
    public List<CaoliuPost> findPostsByColor(int color);

}
