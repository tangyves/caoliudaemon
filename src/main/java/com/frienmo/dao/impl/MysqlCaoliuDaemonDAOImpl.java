package com.frienmo.dao.impl;

import com.frienmo.dao.CaoliuDaemonDAO;
import com.frienmo.model.CaoliuPost;
import com.frienmo.model.CaoliuPostRowMapper;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;

import javax.sql.DataSource;
import java.sql.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;

/**
 * Created by 4 on 2015-10-13.
 */
public class MysqlCaoliuDaemonDAOImpl implements CaoliuDaemonDAO {
    private static final Logger log = Logger.getLogger(MysqlCaoliuDaemonDAOImpl.class);

    @Autowired
    private DataSource dataSource;

    @Autowired
    private JdbcTemplate jdbcTemplate;

    private static final String sqlGetPostByIds = "SELECT * FROM post as p where p.fid = ? and p.ida=? and p.idb=?";
    private static final String sqlUpdatePost = "UPDATE post SET title=?,color=?,note=?,username=? WHERE ida=? and idb=? and fid=?";
    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    private static HashMap<String,Integer> colorStrIntMap = new HashMap<String,Integer>();
    static {
        colorStrIntMap.put("green",103);
        colorStrIntMap.put("red",114);
        colorStrIntMap.put("black",98);
        colorStrIntMap.put("blue",99);
        colorStrIntMap.put("orange",111);
    }

    public DataSource getDataSource() {
        return dataSource;
    }

    @Override
    public int insertPost(final CaoliuPost caoliuPost) {
                KeyHolder keyHolder = new GeneratedKeyHolder();
        try {
            this.jdbcTemplate.update(new PreparedStatementCreator() {
                @Override
                public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
                    String sql = "INSERT INTO post (ida,idb,fid,title,username,date,color,note) VALUES (?,?,?,?,?,?,?,?)";
                    PreparedStatement ps =
                            connection.prepareStatement(sql, new String[]{"id"});
//                    System.out.println(caoliuPost.getIda());
                    ps.setInt(1, caoliuPost.getIda());
                    ps.setInt(2, caoliuPost.getIdb());
                    ps.setInt(3, caoliuPost.getFid());
                    ps.setString(4, caoliuPost.getTitle());
                    ps.setString(5, caoliuPost.getUsername());
                    Date date;
                    SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
                    try {
                        java.util.Date parsed = format.parse(caoliuPost.getDate());
                        date = new Date(parsed.getTime());
                        ps.setDate(6, date);
                        String colorStr = caoliuPost.getColor();
                        Integer colorInt = colorStrIntMap.get(colorStr);
                        if (colorInt == null) {
                            log.warn("Color:" + colorStr + " not in hashmap");
                            colorInt = 1;
                        }
                        ps.setInt(7, colorInt);
                        ps.setInt(8, caoliuPost.getNote());
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    return ps;
                }
            });
            log.info("Inserted:" + caoliuPost);
        } catch (DuplicateKeyException e) {
            log.warn(e);
        }
        return 0;
    }

    @Override
    public CaoliuPost getPostByIds(int fid, int ida, int idb) {
        try {
            CaoliuPost post = (CaoliuPost) jdbcTemplate.queryForObject(sqlGetPostByIds, new Object[]{fid, ida, idb}, new CaoliuPostRowMapper());
            return post;
        } catch (EmptyResultDataAccessException e) {
            return null;
        }
    }

    @Override
    public int updatePost(CaoliuPost caoliuPost) {
//        title=?,color=?,note=?,username=? WHERE ida=? and idb=? and fid=?
        Object[] params = {caoliuPost.getTitle(),(int) caoliuPost.getColor().charAt(0),caoliuPost.getNote(),
                            caoliuPost.getUsername(),caoliuPost.getIda(),caoliuPost.getIdb(),caoliuPost.getFid()};
        int[] types = {Types.VARCHAR,Types.TINYINT,Types.INTEGER,Types.VARCHAR,Types.INTEGER,Types.INTEGER,Types.TINYINT};
        int rows = jdbcTemplate.update(sqlUpdatePost, params, types);
        return rows;
    }

    @Override
    public List<CaoliuPost> findPostsByUserName(String username, int limit) {
        return null;
    }

    @Override
    public List<CaoliuPost> findPostsHighLight(int limit) {
        return null;
    }

    @Override
    public List<CaoliuPost> findPostsByColor(int color) {
        return null;
    }

}
