package com.frienmo.model;

/**
 * Created by yichen on 10/9/15.
 */
public class CaoliuPost {
//    private int id;
    private int ida;
    private int idb;
    private int fid;
    private String title;
    private String username;
    private String date;
    private String color = "black";
    private int note;

    public CaoliuPost() {
    }

    public CaoliuPost(int ida, int idb, int fid, String title, String username, String date, String color,int note) {
        this.ida = ida;
        this.idb = idb;
        this.fid = fid;
        this.title = title;
        this.username = username;
        this.date = date;
        this.color = color;
        this.note = note;
    }

    public int getIda() {
        return ida;
    }

    public void setIda(int ida) {
        this.ida = ida;
    }

    public int getIdb() {
        return idb;
    }

    public void setIdb(int idb) {
        this.idb = idb;
    }

    public int getFid() {
        return fid;
    }

    public void setFid(int fid) {
        this.fid = fid;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public int getNote() {
        return note;
    }

    public void setNote(int note) {
        this.note = note;
    }

    @Override
    public String toString() {
        return "CaoliuPost{" +
                "ida=" + ida +
                ", idb=" + idb +
                ", fid=" + fid +
                ", title='" + title + '\'' +
                ", username='" + username + '\'' +
                ", date='" + date + '\'' +
                ", color='" + color + '\'' +
                ", note=" + note +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CaoliuPost that = (CaoliuPost) o;

        if (fid != that.fid) return false;
        if (ida != that.ida) return false;
        if (idb != that.idb) return false;
        if (note != that.note) return false;
        if (!color.equals(that.color)) return false;
        if (!title.equals(that.title)) return false;
        if (!username.equals(that.username)) return false;
        return true;
    }

    @Override
    public int hashCode() {
        int result = ida;
        result = 31 * result + idb;
        result = 31 * result + fid;
        result = 31 * result + title.hashCode();
        result = 31 * result + username.hashCode();
        result = 31 * result + color.hashCode();
        result = 31 * result + note;
        return result;
    }
}
