package com.frienmo.model;

import org.springframework.jdbc.core.RowMapper;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.HashSet;

/**
 * Created by 4 on 2015-10-14.
 */
public class CaoliuPostRowMapper implements RowMapper {
    private static HashMap<Integer,String> colorIntStrMap = new HashMap<Integer,String>();
    static {
        colorIntStrMap.put(103,"green");
        colorIntStrMap.put(114,"red");
        colorIntStrMap.put(98,"black");
        colorIntStrMap.put(111,"orange");
        colorIntStrMap.put(99,"blue");
    }
    @Override
    public Object mapRow(ResultSet resultSet, int i) throws SQLException {
        CaoliuPost caoliuPost = new CaoliuPost();
        int ida = resultSet.getInt(2);
        int idb = resultSet.getInt(3);
        int fid = resultSet.getInt(4);
        String title = resultSet.getString(5);
        String user = resultSet.getString(6);
        Date date = resultSet.getDate(7);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String dateStr = sdf.format(date);
        int color = resultSet.getInt(8);
        int note = resultSet.getInt(9);
        caoliuPost.setFid(fid);
        caoliuPost.setIda(ida);
        caoliuPost.setIdb(idb);
        caoliuPost.setTitle(title);
        caoliuPost.setUsername(user);
        caoliuPost.setDate(dateStr);
        caoliuPost.setNote(note);
        caoliuPost.setColor(colorIntStrMap.get(color));
        return caoliuPost;
    }
}
