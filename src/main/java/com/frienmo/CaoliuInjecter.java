package com.frienmo;

import com.frienmo.dao.CaoliuDaemonDAO;
import com.frienmo.model.CaoliuPost;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by 4 on 2015-10-14.
 */
public class CaoliuInjecter {

    static private Logger log = Logger.getLogger(CaoliuInjecter.class);

    @Autowired
    CaoliuDaemonDAO caoliuDAO;
    Set<String> localInjectedPostIds = new HashSet<String>();

    private int localSetSize = 10000;
    public void inject(CaoliuPost post) {
        String strIds = post.getFid() + "_" + post.getIda() + "_" + post.getIdb();
        if (localInjectedPostIds.contains(strIds))
            return;
        CaoliuPost existedPost = caoliuDAO.getPostByIds(post.getFid(), post.getIda(), post.getIdb());
        if (existedPost != null) {
            // existed
            localInjectedPostIds.add(strIds);
            if (post.equals(existedPost)) {
                // do nothing
            } else {
                // update
                log.debug("updateing...");
                int rows = caoliuDAO.updatePost(post);
                if(rows == 1) {
                    log.info("Updated:" + post);
                }else {
                    log.warn("Warning updated:" + post);
                    log.warn("Existed post:" + existedPost);
                }
            }
        } else {
            // inject
            if (post.getIda() != 0) {
//            System.out.println(post);
                caoliuDAO.insertPost(post);
                localInjectedPostIds.add(strIds);
                if (localInjectedPostIds.size() > localSetSize) {
                    this.cleanCache();
                }
            }
        }
    }

//    private boolean exists(CaoliuPost post) {
//        CaoliuPost postInDB = caoliuDAO.getPostByIds(post.getFid(), post.getIda(), post.getIdb());
//        if (postInDB != null) {
//            log.debug(postInDB.getTitle());
//            return true;
//        }
//        return false;
//    }

    public void cleanCache() {
        localInjectedPostIds = new HashSet<String>();
    }

    public int getLocalSetSize() {
        return localSetSize;
    }

    public void setLocalSetSize(int localSetSize) {
        this.localSetSize = localSetSize;
    }
}
