package com.frienmo;

import com.frienmo.model.CaoliuPost;
import org.apache.log4j.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.List;

/**
 * Created by 4 on 2015-10-08.
 */
public class CaoLiuCrawlerLauncher {

    private static Logger log = Logger.getLogger(CaoLiuCrawlerLauncher.class);
    static CaoLiuCrawler caoliuCrawler;
    static CaoliuInjecter caoliuInjecter;
    public static void main(String[] args) {
//        CaoLiuCrawler caoLiuCrawler = new CaoLiuCrawler();
        ApplicationContext context = new ClassPathXmlApplicationContext("spring-config.xml");
        caoliuCrawler = (CaoLiuCrawler) context.getBean("caoliuCrawler");
        caoliuInjecter = (CaoliuInjecter) context.getBean("caoliuInjecter");
        int i = 0;
        while (true) {
            ++i;
            startRound(i);
//            testUpdate();
            try {
                Thread.sleep(1000 * 60 * 5);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
//            System.out.println("done");
        }
    }

    private static void startRound(int i) {
        log.info("start round:" + i);
        List<CaoliuPost> posts = caoliuCrawler.getPosts();
        for(CaoliuPost p : posts) {
            caoliuInjecter.inject(p);
        }
        caoliuCrawler.cleanResults();
        log.info("stop round:" + i);
    }

    private static void testUpdate() {
        System.out.println("--------------");
        CaoliuPost p = new CaoliuPost();
        int ida = 1510;
        int idb = 1661108;
        int fid = 7;
        p.setIda(ida);
        p.setIdb(idb);
        p.setFid(fid);
        p.setNote(40);
        p.setTitle("til");
        p.setUsername("us");
        p.setColor("red");
        p.setDate("2015-12-05");
        caoliuInjecter.inject(p);
    }
}
